//
//  CGFloatExtensions.swift
//  RotateFill
//
//  Created by Wani Wang on 2017/10/13.
//  Copyright © 2017年 Wani Wang. All rights reserved.
//

import UIKit

extension CGFloat {
    func almostEqual(_ other:CGFloat, threshold:CGFloat = 0.000001) -> Bool {
        return fabs(self - other) < threshold
    }
}

//
//  UIViewExtension.swift
//  RotateFill
//
//  Created by Wani Wang on 2017/10/13.
//  Copyright © 2017年 Wani Wang. All rights reserved.
//

import UIKit

enum CGRectVertexType {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
}

extension UIView {
    func rectForPoints(_ points:[CGPoint]) -> CGRect {
        var minX:CGFloat?
        var minY:CGFloat?
        var maxX:CGFloat?
        var maxY:CGFloat?
        
        points.forEach { point in
            minX = minX == nil ? point.x : min(minX!, point.x)
            minY = minY == nil ? point.y : min(minY!, point.y)
            maxX = maxX == nil ? point.x : max(maxX!, point.x)
            maxY = maxY == nil ? point.y : max(maxY!, point.y)
        }
        
        return CGRect(x: minX!,
                      y: minY!,
                      width: maxX! - minX!,
                      height: maxY! - minY!)
    }

    
    func pic_isFullyCovered(by other:UIView) -> Bool {
        let topLeftOnOther = convert(bounds.topLeftPoint, to: other)
        let topRightOnOther = convert(bounds.topRightPoint, to: other)
        let bottomLeftOnOther = convert(bounds.bottomLeftPoint, to: other)
        let bottomRightOnOther = convert(bounds.bottomRightPoint, to: other)
        return other.bounds.pic_contains(topLeftOnOther) &&
            other.bounds.pic_contains(topRightOnOther) &&
            other.bounds.pic_contains(bottomLeftOnOther) &&
            other.bounds.pic_contains(bottomRightOnOther)
    }
    
    func rect(on view:UIView) -> CGRect {
        let topLeft = convert(bounds.topLeftPoint, to: view)
        let topRight = convert(bounds.topRightPoint, to: view)
        let bottomLeft = convert(bounds.bottomLeftPoint, to: view)
        let bottomRightTop = convert(bounds.bottomRightPoint, to: view)
        let rect = rectForPoints([topLeft, topRight, bottomLeft, bottomRightTop])
        return rect
    }
    
    func scaleForFilling(view:UIView) -> CGFloat {
        let rect = view.rect(on: self)
        let scale = max(rect.width / bounds.width, rect.height / bounds.height)
        return scale
    }
    
    func translationForFilling(view: UIView) -> CGPoint {
        let rect = view.rect(on: self)
        
        let rotationAngle = transform.rotationAngle
        let scale = transform.scale
        var translation = bounds.translationForFitting(rect: rect)
        
        translation = translation.applying(CGAffineTransform(rotationAngle: rotationAngle))
        translation = translation * scale
        
        return translation
    }
}

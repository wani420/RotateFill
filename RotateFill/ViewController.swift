//
//  ViewController.swift
//  RotateFill
//
//  Created by Wani Wang on 2017/10/11.
//  Copyright © 2017年 Wani Wang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let bottomView = UIView()
    let topView = UIView()
    let shapeLayer = CAShapeLayer()
    
    let targetRectLayer = CAShapeLayer()
    
    var rotationAngle:CGFloat = 0 {
        didSet {
            drawTargetRect()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        addBottomView()
        addTopView()
        addShapeLayer()
        addTargetRectLayer()
        let rotationAngle = CGFloat(Double.pi * 1.5/4)
        
        rotateTopRectAndFill(radian: rotationAngle)
        self.rotationAngle = rotationAngle
    }
    
    func getTargetPoints() -> (tl:CGPoint, tr:CGPoint, bl:CGPoint, br:CGPoint) {
        let topLeftTopView = bottomView.convert(bottomView.bounds.topLeftPoint, to: topView)
        let topRightTopView = bottomView.convert(bottomView.bounds.topRightPoint, to: topView)
        let bottomLeftTopView = bottomView.convert(bottomView.bounds.bottomLeftPoint, to: topView)
        let bottomRightTopView = bottomView.convert(bottomView.bounds.bottomRightPoint, to: topView)
        
        let rect = rectForPoints([topLeftTopView, topRightTopView, bottomLeftTopView, bottomRightTopView])
        
        let tl = topView.convert(rect.topLeftPoint, to: view)
        let tr = topView.convert(rect.topRightPoint, to: view)
        let bl = topView.convert(rect.bottomLeftPoint, to: view)
        let br = topView.convert(rect.bottomRightPoint, to: view)
        
        return (tl:tl, tr:tr, bl:bl, br:br)
    }
    
    func drawTargetRect() {
        let topLeftTopView = bottomView.convert(bottomView.bounds.topLeftPoint, to: topView)
        let topRightTopView = bottomView.convert(bottomView.bounds.topRightPoint, to: topView)
        let bottomLeftTopView = bottomView.convert(bottomView.bounds.bottomLeftPoint, to: topView)
        let bottomRightTopView = bottomView.convert(bottomView.bounds.bottomRightPoint, to: topView)
        
        let rect = rectForPoints([topLeftTopView, topRightTopView, bottomLeftTopView, bottomRightTopView])
        
        let tl = topView.convert(rect.topLeftPoint, to: view)
        let tr = topView.convert(rect.topRightPoint, to: view)
        let bl = topView.convert(rect.bottomLeftPoint, to: view)
        let br = topView.convert(rect.bottomRightPoint, to: view)
        
        let path = UIBezierPath()
        path.move(to: tl)
        path.addLine(to: tr)
        path.addLine(to: br)
        path.addLine(to: bl)
        path.close()
        targetRectLayer.path = path.cgPath
    }
    
    func rectForPoints(_ points:[CGPoint]) -> CGRect {
        var minX:CGFloat?
        var minY:CGFloat?
        var maxX:CGFloat?
        var maxY:CGFloat?
        
        points.forEach { point in
            minX = minX == nil ? point.x : min(minX!, point.x)
            minY = minY == nil ? point.y : min(minY!, point.y)
            maxX = maxX == nil ? point.x : max(maxX!, point.x)
            maxY = maxY == nil ? point.y : max(maxY!, point.y)
        }
        
        return CGRect(x: minX!,
                      y: minY!,
                      width: maxX! - minX!,
                      height: maxY! - minY!)
    }
    
    func addTargetRectLayer() {
        view.layer.addSublayer(targetRectLayer)
        targetRectLayer.fillColor = UIColor.green.withAlphaComponent(0.3).cgColor
    }
    
    func addShapeLayer() {
        view.layer.addSublayer(shapeLayer)
        shapeLayer.lineWidth = 2
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.strokeColor = UIColor.red.cgColor
    }
    
    func addBottomView() {
        bottomView.backgroundColor = UIColor.yellow
        view.addSubview(bottomView)
        bottomView.bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
        bottomView.center = view.center
    }
    
    func addTopView() {
        topView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        view.addSubview(topView)
        topView.bounds = CGRect(x: 0, y: 0, width: 200, height: 100)
        topView.center = bottomView.center
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        topView.addGestureRecognizer(pan)
    }
    
    @objc func handlePan(gesture:UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        gesture.setTranslation(.zero, in: view)

        topView.center = topView.center + translation
        
        if gesture.state == .ended {
            let translation = topView.translationForFilling(view: bottomView)
            topView.center = topView.center + translation
        }
        
    }
    
    func updateCenterLine() {
        let path = UIBezierPath()
        path.move(to: topView.center)
        path.addLine(to: bottomView.center)
        shapeLayer.path = path.cgPath
    }
    
    func rotateTopRectAndFill(radian:CGFloat) {
        topView.transform = topView.transform.rotated(by: radian)
        let scale = topView.scaleForFilling(view: bottomView)
        topView.transform = topView.transform.scaledBy(x: scale, y: scale)
    }
    
    func addDot(at point:CGPoint, on view:UIView, color:UIColor = UIColor.red) {
        let dot = UIView()
        dot.backgroundColor = UIColor.red
        dot.bounds = CGRect(x: 0, y: 0, width: 4, height: 4)
        dot.center = point
        view.addSubview(dot)
    }
}

func *(l:CGPoint, r:CGFloat) -> CGPoint {
    return CGPoint(x: l.x * r, y: l.y * r)
}

extension CGRect {
    func translationForFitting( rect:CGRect) -> CGPoint {
        
        let dL = rect.minX - minX      // Always negative
        let dR = rect.maxX - maxX      // Always positive
        
        let dX = dL < -dR ? min(0, dL) : max(0, dR)
        
        let dT = rect.minY - minY      // Always negative
        let dB = rect.maxY - maxY      // Always positive
        
        let dY = dT < -dB ? min(0, dT) : max(0, dB)
        
        return CGPoint(x: dX, y: dY)
    }
}



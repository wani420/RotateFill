//
//  CGRectExtension.swift
//  RotateFill
//
//  Created by Wani Wang on 2017/10/13.
//  Copyright © 2017年 Wani Wang. All rights reserved.
//

import UIKit

extension CGRect {
    var topLeftPoint:CGPoint {
        return CGPoint(x: minX, y: minY)
    }
    var bottomLeftPoint:CGPoint {
        return CGPoint(x: minX, y: maxY)
    }
    var topRightPoint:CGPoint {
        return CGPoint(x: maxX, y: minY)
    }
    var bottomRightPoint:CGPoint {
        return CGPoint(x: maxX, y: maxY)
    }
    
    func pic_contains(_ point: CGPoint) -> Bool{
        return (point.x > minX || point.x.almostEqual(minX)) &&
            (point.x < maxX || point.x.almostEqual(maxX)) &&
            (point.y > minY || point.y.almostEqual(minY)) &&
            (point.y < maxY || point.y.almostEqual(maxY))
    }
}


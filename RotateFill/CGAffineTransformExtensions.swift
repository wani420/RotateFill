//
//  CGAffineTransformExtensions.swift
//  RotateFill
//
//  Created by Wani Wang on 2017/10/13.
//  Copyright © 2017年 Wani Wang. All rights reserved.
//

import UIKit

extension CGAffineTransform {
    var rotationAngle:CGFloat {
        return CGFloat(atan2f(Float(b), Float(a)))
    }
    var scale:CGFloat {
        return sqrt(a * a + c * c)
    }
}

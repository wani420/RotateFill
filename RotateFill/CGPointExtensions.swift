//
//  CGPointExtension.swift
//  RotateFill
//
//  Created by Wani Wang on 2017/10/13.
//  Copyright © 2017年 Wani Wang. All rights reserved.
//

import UIKit

extension CGPoint {
    func rotate(angle:CGFloat, around center:CGPoint) -> CGPoint {
        let newPoint = self.applying(CGAffineTransform(translationX: -center.x, y: -center.y))
        newPoint.applying(CGAffineTransform(rotationAngle: angle))
        newPoint.applying(CGAffineTransform(translationX: center.x, y: center.y))
        return newPoint
    }
}

func -(left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func +(left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

